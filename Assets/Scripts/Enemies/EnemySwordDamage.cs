using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySwordDamage : MonoBehaviour
{
    [SerializeField] private Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if (animator.GetBool("attack"))
            {
                LifeController playerLifeController = GameObject.Find("Player").GetComponent<LifeController>();
                playerLifeController.InvokeDamageEvent();
                Debug.Log("Da�o al jugador");
            }
            
        }
    }
}
