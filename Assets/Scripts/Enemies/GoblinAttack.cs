using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinAttack : MonoBehaviour
{
    private int layerMask;
    private Animator goblinAnimator;
    private Transform target;
    void Start()
    {
        goblinAnimator = GetComponentInParent<Animator>();
        layerMask = 3;
    }

    void FixedUpdate()
    {
        

        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward,out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);

            if (hit.collider.tag == "Player")
            {
                goblinAnimator.SetBool("Attack", true);
                target = hit.transform;
                transform.LookAt(target.position);
                Debug.Log("Hit con player");
            }
            else
            {
                goblinAnimator.SetBool("Attack", false);
                target = null;
            }
        }
    }
}
