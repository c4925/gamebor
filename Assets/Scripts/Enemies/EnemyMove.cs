using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    public float speedMove;
    public int rutine;
    public float timer;
    public Animator ani;
    public Quaternion angle;
    public float grades;
    public GameObject target;
    public bool isAttack;
    public LifeController playerLife;
    // Start is called before the first frame update
    void Start()
    {
        ani = GetComponent<Animator>();
        target = GameObject.Find("Player");
        playerLife = target.GetComponent<LifeController>();
    }

    // Update is called once per frame
    void Update()
    {
        EnemyBasicMove();
    }

    void EnemyBasicMove()
    {
        if (Vector3.Distance(transform.position, target.transform.position) > 15 || playerLife.IsDie())
        {
            ani.SetBool("walk", false);
            timer += 1 * Time.deltaTime;
            if (timer >= 4)
            {
                rutine = Random.Range(0, 2);
                timer = 0;
            }

            switch (rutine)
            {
                case 0:
                    ani.SetBool("walk", false);
                    break;
                case 1:
                    grades = Random.Range(0, 360);
                    angle = Quaternion.Euler(0, grades, 0);
                    rutine++;
                    break;
                case 2:
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, angle, 0.5f);
                    transform.Translate(Vector3.forward * speedMove * Time.deltaTime);
                    ani.SetBool("walk", true);
                    break;
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, target.transform.position) > 4 && !isAttack)
            {
                Vector3 lookPos = target.transform.position - transform.position;
                lookPos.y = 0;
                Quaternion rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 2);
                ani.SetBool("walk", true);

                transform.Translate(Vector3.forward * speedMove * Time.deltaTime);

                ani.SetBool("attack", false);
            }
            else
            {
                ani.SetBool("walk", false);

                ani.SetBool("attack", true);
                isAttack = true;
            }
        }
    }

    public void EndAttack()
    {
        ani.SetBool("attack", false);
        isAttack = false;
    }

    private void OnDestroy()
    {
        MisionUI misionUI = GameObject.Find("Quest").GetComponent<MisionUI>();
        if (misionUI != null)
        {
            Debug.Log("Enemy invoca a Enemikill event");
            misionUI.EnemyKillInvoke();
            Debug.Log("Enemy invoca a Mision complete event");
            misionUI.MisionCompleteInvoke();
        }
    }
}
