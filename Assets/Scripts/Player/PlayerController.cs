using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float horizontalMove;
    private float verticalMove;
    //private CharacterController player;
    private Vector3 playerInput;
    [SerializeField] private float playerSpeed;
    [SerializeField] private Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight;
    private Vector3 movePlayer;
    //[SerializeField] float gravity = 9.8f;
    private float fallVelocity;
    private Animator anim;
    [SerializeField] ParticleSystem pasos;

    // Start is called before the first frame update
    void Start()
    {
        //player = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        pasos.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        playerInput = Vector3.ClampMagnitude(GetDirection(),1);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (playerSpeed <= 2)
            {
                playerSpeed += Time.deltaTime;
            }

            if(playerSpeed >= 2)
            {
                if (GetDirection().magnitude > 0)
                    pasos.Play();
                else
                    pasos.Stop();
            }
                
        }
        else
        {
            pasos.Stop();
            if (playerSpeed > 1)
                playerSpeed -= Time.deltaTime;
        }

        anim.SetFloat("PlayerSpeed", playerInput.magnitude * playerSpeed);

        CameraDirection();

        movePlayer = playerInput.x * camRight + playerInput.z * camForward;
        
        movePlayer = movePlayer * playerSpeed;


        transform.LookAt(transform.position + movePlayer);
    }

    private Vector3 GetDirection()
    {
        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");
        return new Vector3(horizontalMove, 0,verticalMove);
    }

    private void CameraDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;

        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }

}
