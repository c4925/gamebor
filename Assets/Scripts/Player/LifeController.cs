using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LifeController : MonoBehaviour
{
    private Action onDamage;
    public int life;
    public Animator playerAnimator;
    private PlayerLifeUI playerLifeUI;

    // Start is called before the first frame update
    void Start()
    {
        playerAnimator = GetComponent<Animator>();
        playerLifeUI = GameObject.Find("PlayerLife").GetComponent<PlayerLifeUI>();
        onDamage += OnDamageHelper;
        life = 3;
    }

    public void InvokeDamageEvent()
    {
        onDamage?.Invoke();
    }

    public void OnDamageHelper()
    {
        if (life > 0)
        {
            life--;
            playerLifeUI.InvokeDamangeUI();
        }
            

        if (life == 0 && !playerAnimator.GetBool("IsDie"))
            playerAnimator.SetBool("IsDie", true);

        if (IsDie())
        {
            Debug.Log("GameOver");
            GameManager.instance.SetCurrentState(GameManager.gameState.GameOver);
        }
    }

    public int Life()
    {
        return life;
    }

    public bool IsDie()
    {
        return life == 0;
    }
}
