using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordDamage : MonoBehaviour
{
    private Animator playerAnimator;
    // Start is called before the first frame update
    void Start()
    {
        Transform playerTransform = GameObject.Find("Player").transform;
        playerAnimator = playerTransform.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && playerAnimator.GetInteger("attack") > 0)
        {
            
            other.gameObject.GetComponent<Animator>().SetTrigger("Fall1");
            other.GetComponent<Animator>().SetBool("attack", false);
            Destroy(other.gameObject,3.0f);
        }
    }
}
