using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    private Animator anim;

    private int noOfClicks;
    private bool canAttack;

    private AudioSource attackSound;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        attackSound = GetComponent<AudioSource>();
        noOfClicks = 0;
        canAttack = true;
    }

    // Update is called once per frame
    void Update()
    {
        Inputs();   
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && anim.GetInteger("attack") > 0)
        {
            other.GetComponent<Animator>().SetTrigger("Fall1");
            Destroy(other.gameObject,3f);
        }
    }

    private void Inputs()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCombo();

            attackSound.Play();
        }
    }

    private void StartCombo()
    {
        if (canAttack)
            noOfClicks++;
        

        if(noOfClicks == 1)
            anim.SetInteger("attack", 1);
    }

    public void ValidateCombo()
    {
        canAttack = false;

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attack1") && noOfClicks == 1)
        {
            anim.SetInteger("attack", 0);
            canAttack = true;
            noOfClicks = 0;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attack1") && noOfClicks >= 2)
        {
            anim.SetInteger("attack", 2);
            canAttack = true;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attack2") && noOfClicks == 2)
        {
            anim.SetInteger("attack", 0);
            canAttack = true;
            noOfClicks = 0;
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attack2") && noOfClicks >= 3)
        {
            anim.SetInteger("attack", 3);
            canAttack = true;
        }
        else if(anim.GetCurrentAnimatorStateInfo(0).IsName("Attack3"))
        {
            anim.SetInteger("attack", 0);
            canAttack = true;
            noOfClicks = 0;
        }
    }
}
