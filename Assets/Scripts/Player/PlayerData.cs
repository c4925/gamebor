using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewPlayerData",menuName ="Player/Data",order =1)]
public class PlayerData : ScriptableObject
{
    public int lifes;
}
