using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public void NewGame()
    {
        Debug.Log("Juego Nuevo");
        GameManager.instance.ResetLevel();
    }

    public void Options()
    {
        Debug.Log("Opciones");
    }

    public void Credits()
    {
        Debug.Log("Mostrando creditos");
        GameManager.instance.SetCurrentState(GameManager.gameState.Win);
    }

    public void Exit()
    {
        Debug.Log("Saliendo del juego...");
        Application.Quit();
    }
}
