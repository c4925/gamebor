using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverMenuController : MonoBehaviour
{
    public void Continue()
    {
        GameManager.instance.ResetLevel();
    }

    public void MainMenu()
    {
        GameManager.instance.SetCurrentState(GameManager.gameState.Init);
    }
}
