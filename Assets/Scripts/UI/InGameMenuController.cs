using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameMenuController : MonoBehaviour
{
    [SerializeField]
    private GameObject go;

    private void Start()
    {

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            go.SetActive(!go.activeSelf);

            if (!go.activeSelf)
            {
                GameManager.instance.SetCurrentState(GameManager.gameState.Play);
            }
            else
            {
                GameManager.instance.SetCurrentState(GameManager.gameState.Pause);
            }
        }
    }

    public void Continue()
    {
        GameManager.instance.SetCurrentState(GameManager.gameState.Play);
        go.SetActive(false);
    }

    public void MainMenu()
    {
        GameManager.instance.SetCurrentState(GameManager.gameState.Init);
    }
}
