using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayerLifeUI : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabHeart;

    private List<Image> imagesHeart;

    [SerializeField][Range(1, 20)]
    private int MAX_LIFE;

    private RectTransform rc;


    private Action onDamage;

    // Start is called before the first frame update
    void Start()
    {
        onDamage += OnDamageUIHelper;

        rc = gameObject.GetComponent<RectTransform>();

        imagesHeart = new List<Image>();
        Image[] images = rc.GetComponentsInChildren<Image>();
        for(int i = 0; i < images.Length; i++)
            imagesHeart.Add(images[i]);
        
        
    }

    public void InvokeDamangeUI()
    {
        onDamage?.Invoke();
    }

    private void OnDamageUIHelper()
    {
        RemoveLife();
    }

    private void AddLife()
    {
        if(imagesHeart.Count < MAX_LIFE)
        {
            Image heart = Instantiate(prefabHeart).GetComponent<Image>();
            imagesHeart.Add(heart);
            heart.rectTransform.SetParent(rc);
        }
    }

    private void RemoveLife()
    {
        if (imagesHeart.Count > 0)
        {
            Image heart = imagesHeart[imagesHeart.Count - 1];
            imagesHeart.Remove(heart);
            Destroy(heart);
            
        }
    }
}
