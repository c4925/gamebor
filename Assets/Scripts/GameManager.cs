using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public enum gameState
    {
        Init,
        Play,
        Pause,
        GameOver,
        Win
    }

    public gameState currentState;
    private int level;

    private const string MENU_SCENE_NAME = "MenuScene";
    private const string GAME_OVER_SCENE_NAME = "GameOverScene";
    private const string LEVEL_1_SCENE_NAME = "Level1Scene";
    private const string LEVEL_2_SCENE_NAME = "Level2Scene";
    private const string LEVEL_3_SCENE_NAME = "Level3Scene";
    private const string CREDIT_SCENE_NAME = "CreditsScene";

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case gameState.Init:
                ChangeScene(MENU_SCENE_NAME);
                SetLevel(1);
                SetCurrentState(gameState.Play);
                break;
            case gameState.Play:
                Time.timeScale = 1.0f;
                break;
            case gameState.Pause:
                Time.timeScale = 0;
                break;
            case gameState.GameOver:
                ChangeScene(GAME_OVER_SCENE_NAME);
                SetCurrentState(gameState.Pause);
                break;
            case gameState.Win:
                ChangeScene(CREDIT_SCENE_NAME);
                SetCurrentState(gameState.Play);
                break;
        }
    }

    public void SetCurrentState(gameState state)
    {
        currentState = state;
    }

    public gameState GetCurrentState()
    {
        return currentState;
    }


    private void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void SetLevel(int level)
    {
        this.level = level;
    }

    public void ResetLevel()
    {
        LoadSceneLevel();
        SetCurrentState(gameState.Play);
    }

    private void LoadSceneLevel()
    {
        switch (level)
        {
            case 1:
                ChangeScene(LEVEL_1_SCENE_NAME);
                break;
            case 2:
                ChangeScene(LEVEL_2_SCENE_NAME);
                break;
            case 3:
                ChangeScene(LEVEL_3_SCENE_NAME);
                break;
            case -1:
                SetCurrentState(gameState.Win);
                break;
        }
    }

    public void NextLevel()
    {
        switch (level)
        {
            case 1:
                SetLevel(2);
                break;
            case 2:
                SetLevel(3);
                break;
            case 3:
                SetLevel(-1);
                break;
        }
        LoadSceneLevel();
    }
}
