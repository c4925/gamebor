using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MisionUI : MonoBehaviour
{
    [SerializeField] private Text misionText;
    [SerializeField] private int maxEnemies;
    [SerializeField] private int actualEnemies;
    [SerializeField] private Toggle misionComplete;
    private Action onEnemyKill;
    private Action onMisionComplete;
    // Start is called before the first frame update
    void Start()
    {
        onEnemyKill += OnEnemyKillHandler;
        onMisionComplete += OnMisionCompleteHandler;
        onMisionComplete += NextLevelHandler;
        misionComplete.isOn = false;
        actualEnemies = 0;
        misionText.text = actualEnemies.ToString() + "/" + maxEnemies.ToString();
    }

    public void EnemyKillInvoke()
    {
        onEnemyKill?.Invoke();
    }

    public void MisionCompleteInvoke()
    {
        onMisionComplete?.Invoke();
    }

    private void OnEnemyKillHandler()
    {
        actualEnemies++;
        misionText.text = actualEnemies.ToString() + "/" + maxEnemies.ToString();
    }

    private void OnMisionCompleteHandler()
    {
        if(actualEnemies == maxEnemies)
        {
            misionComplete.isOn = true;
        }
    }

    private void NextLevelHandler()
    {
        if (misionComplete.isOn)
        {
            GameManager.instance.NextLevel();
        }
    }
}
