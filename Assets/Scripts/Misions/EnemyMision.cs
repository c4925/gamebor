using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMision : Mision
{
    private int enemiesToKill;
    [SerializeField] private GameObject prefabEnemy;
    private GameObject[] enemies;
    [SerializeField] private Transform[] positionEnemies;
    

    // Start is called before the first frame update
    void Start()
    {
        enemiesToKill = 5;
        enemies = new GameObject[enemiesToKill];
        GenerateEnemies();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override bool MisionComplete()
    {
       return enemies.Length == 0;
    }

    private void GenerateEnemies()
    {
        Debug.Log("Creando Enemigos");
        for (int i = 0; i < enemiesToKill; i++)
        {
            Debug.Log("Enemigo nro " + i);
            Instantiate(prefabEnemy, positionEnemies[i].position, positionEnemies[i].rotation, positionEnemies[i]);
        }
    }
}
